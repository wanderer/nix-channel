let
  pinnedPkgs = import ./pinned-pkgs.nix;

  overlays = [];
  config = {};

  pkgs = pinnedPkgs {
    inherit overlays;
  };
in pkgs
