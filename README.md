# nix-channel

a personal `nix-channel` (read [more about Nix
channels](https://nixos.wiki/wiki/Nix_channels)) managed using
[`niv`](https://github.com/nmattia/niv), see [this Luc Perkins's
post](https://lucperkins.dev/blog/nix-channel/) for reference

### LICENSE
WTFPL

<!--vim: tw=80-->
